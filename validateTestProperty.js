'use strict';

/**
 * Validate phonenumber object property
 *
 * @param {Object} [phoneNumObj] - Phone number object
 * @param {String} [property] - Phone number object property
 * @returns {Object}
 */
module.exports = function(phoneNumObj, property) {
  return !phoneNumObj.hasOwnProperty(property) ||
    phoneNumObj[property].trim() === ''
    ? true
    : false;
};
