# Simple Phone Validation

![picture](screenshot.png)

## Overview

This is a simple phone validation application with 4 parts:

1. A backend server with validation functions.
2. A Swagger spec generated documentation page for testing the the service manually.
3. A simple application form UI served statically via EJS and jQuery for realistic empirical testing.
4. A Cucumber powered BDD test suite to test the 3 supported scenarios.

### Running the server

To install the project dependencies and run the server, run:

```
npm start
```

### View the documentation

To view the Swagger UI generated documentation, navigate to this URL in a web browser:

```
http://localhost:8080/docs
```

### View the application

To view the application UI, navigate to this URL in a web browser:

```
http://localhost:8080
```

### Running the BDD tests

To install the project dependencies and run the BDD test scenarios with Cucumber, run:

```
npm test
```
