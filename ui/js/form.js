var $form = $('form');
var showAlert = function(type, message) {
  $form.prepend('<div class="col-md-2"></div><div class="col-md-8"><div style="font-size: 14px;" class="alert alert-' + type + '">' + message + '</div></div><div class="col-md-2"></div><div class="clearfix"></div>');
};

// Process form
$form.submit(function(e) {
  $.ajax({
    type: 'post',
    url: '/',
    contentType: 'application/json',
    data: JSON.stringify({
      areaCode: $form.find('input[name=area]').val(),
      preFix: $form.find('input[name=prefix]').val(),
      lineNumber: $form.find('input[name=line]').val(),
      phoneType: $form.find('input[name=type]:checked').val()
    }),
    beforeSend: function() {
      // Remove old alerts
      $form.find('.alert').remove();
    }
  })
    .done(function(data) {
      // Show alert message
      showAlert('success', data.message)
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      // Show alert message
      showAlert('danger', jqXHR.responseJSON.message);
    });

    e.preventDefault();
});
