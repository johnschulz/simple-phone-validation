'use-strict';

var cucumber = require('cucumber');
var fetch = require('node-fetch');
var _ = require('lodash');
var validateTestProperty = require(process.cwd() + '/validateTestProperty');
var httpMocks = {
  missingAreaCode: {
    areaCode: '',
    preFix: '867',
    lineNumber: '5309',
    phoneType: 'Business'
  },
  missingPreFix: {
    areaCode: '123',
    preFix: '  ',
    lineNumber: '5309',
    phoneType: 'Business'
  },
  missingLineNumber: {
    areaCode: '123',
    preFix: '867',
    phoneType: 'Business'
  }
};

module.exports = cucumber.defineSupportCode(function() {
  var Given = cucumber.Given;
  var When = cucumber.When;
  var Then = cucumber.Then;
  var BeforeAll = cucumber.BeforeAll;
  var AfterAll = cucumber.AfterAll;
  var server;

  BeforeAll(function(done) {
    // Get responses from the actual service for each scenario
    server = require('../../index').startServer;
    done();
  });

  Given('A phonenumber object', function(done) {
    if (httpMocks.missingAreaCode && _.isObject(httpMocks.missingAreaCode)) {
      done();
    }
  });

  When('the areaCode field does not have a value', function(done) {
    if (validateTestProperty(httpMocks.missingAreaCode, 'areaCode')) {
      done();
    }
  });

  When('the preFix field does not have a value', function(done) {
    if (validateTestProperty(httpMocks.missingPreFix, 'preFix')) {
      done();
    }
  });

  When('the lineNumber field does not have a value', function(done) {
    if (validateTestProperty(httpMocks.missingLineNumber, 'lineNumber')) {
      done();
    }
  });

  Then('The error {string} will be returned', function(string, done) {
    var key = '';
    switch (string) {
      case 'The areaCode must not be empty':
        key = 'missingAreaCode';
        break;
      case 'The preFix must not be empty':
        key = 'missingPreFix';
        break;
      case 'The lineNumber must be valid':
        key = 'missingLineNumber';
        break;
    }
    // Make the actual http request and test the response
    var opts = {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(httpMocks[key])
    };
    fetch('http://localhost:8080', opts)
      .then(function(req) {
        return req.json();
      })
      .then(function(res) {
        if (res.message === string) {
          done();
        }
      });
  });

  AfterAll(function(done) {
    server = require('../../index').stopServer();
    done();
  });
});
