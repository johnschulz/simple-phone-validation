Feature: When a user submits their basic information
  the user needs to be informed if the phone number is valid.

  Scenario: We have recieved a message that does not have a area code
    Given A phonenumber object
    When the areaCode field does not have a value
    Then The error "The areaCode must not be empty" will be returned

  Scenario: We have recieved a message that does not have a preFix
    Given A phonenumber object
    When the preFix field does not have a value
    Then The error "The preFix must not be empty" will be returned

  Scenario: We have recieved a message that does not have a well formed lineNumber
    Given A phonenumber object
    When the lineNumber field does not have a value
    Then The error "The lineNumber must be valid" will be returned
