'use strict';

exports.validatePhone = function(args, res, next) {
  /**
   * Validate Phone Number
   *
   * @param {String} [areaCode] - 3 digit string
   * @param {String} [preFix] - 3 digit string
   * @param {String} [lineNumber] - 4 digit string
   * @param {String} [phoneType] - Optional descriptive title for phone number
   * @returns {JSON}
   */

  var error = false;
  var msg = { message: 'Phone Valid' };
  var validateInput = function(property, errorMessage) {
    // Upon receiving an invalid phonenumber object property, send 400 error with message
    if (
      !args.phone.value.hasOwnProperty(property) ||
      args.phone.value[property].trim() === ''
    ) {
      // Build error response
      error = true;
      msg.message = errorMessage;
    }
  };

  // Run the validation
  validateInput('areaCode', 'The areaCode must not be empty');
  validateInput('preFix', 'The preFix must not be empty');
  validateInput('lineNumber', 'The lineNumber must be valid');

  // send response
  if (error) {
    res.statusCode = 400;
  }
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(msg));
};
