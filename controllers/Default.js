'use strict';
var url = require('url');
var Default = require('./DefaultService');

module.exports.validatePhone = function validatePhone(req, res, next) {
  Default.validatePhone(req.swagger.params, res, next);
};
