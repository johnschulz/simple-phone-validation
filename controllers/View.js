'use strict';

var ejs = require('ejs');
var fs = require('fs');
var tplStr = fs.readFileSync('./ui/index.ejs', 'utf-8');

module.exports.view = function view(req, res, next) {
  res.end(ejs.render(tplStr));
};
